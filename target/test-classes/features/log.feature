Feature: Login functionality of DemoWebShop    

Scenario: Verify the login functionality

Given user should be at demowebshop application
Then user be at home page of application
When click on the login option
And user should enter the valid "<EMAIL>" in textbox
And user should enter the valid "<PASSWORD>" in text box
And click on login button

And click on logout option
Then  The test is Ended close the browser
 
 Examples:
 |EMAIL                  | |PASSWORD  |
 |jehinwaise007@gmail.com| |jehin@2020|
 |jehinwaise007@gmail.com| |jehin@2020|