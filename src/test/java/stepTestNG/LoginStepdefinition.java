package stepTestNG;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepdefinition {
	static WebDriver driver=null;
	
	@Given("user should be at demowebshop application")
	public void user_should_be_at_demowebshop_application() {
		System.out.println("opening the browser");
	    driver = new ChromeDriver();
	    driver.get("https://demowebshop.tricentis.com/");
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    
	    
	}

	@Then("user be at home page of application")
	public void user_be_at_home_page_of_application() {
		System.out.println("homepage");
	    
	}

	@When("click on the login option")
	public void click_on_the_login_option() {
		System.out.println("click on the login button");
	    driver.findElement(By.linkText("Log in")).click();
	    
	}

	@When("user should enter the valid {string} in textbox")
	public void user_should_enter_the_valid_in_textbox(String string) {
		  System.out.println("enter the email");
		    driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("user should enter the valid {string} in text box")
	public void user_should_enter_the_valid_in_text_box(String string) {
		 System.out.println("enter the password");
		    driver.findElement(By.id("Password")).sendKeys(string);
	    
	}

	@When("click on login button")
	public void click_on_login_button() {
		System.out.println("click on the login button");
	    driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@When("click on logout option")
	public void click_on_logout_option() {
		 System.out.println("click on the logout button");
		    driver.findElement(By.linkText("Log out")).click();
	    
	}

	@Then("The test is Ended close the browser")
	public void the_test_is_ended_close_the_browser() {
		System.out.println("user should be application homepage");
	    System.out.println(driver.getTitle());
	    driver.close();
	    
	    
	}


}


