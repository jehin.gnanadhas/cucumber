package stepTestNG;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
@CucumberOptions(tags = "", features = "src/test/resources/features/log.feature",
glue = "stepTestNG",
plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)


public class TestRunner extends AbstractTestNGCucumberTests{
	

}
